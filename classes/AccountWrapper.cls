public class AccountWrapper{
	public String accountid{get; private set;}
	public String fname{get; private set;}
	public String lname{get; private set;}
	public String email{get; private set;}
	public String phone{get; private set;}
	public String blockheader{get; private set;}
	public DateTime createddate{get; private set;}
	public boolean isselected{get; set;}
	public String communityName{get; private set;}

	public AccountWrapper(String fname, 
						  String lname, 
						  String email, 
						  String phone, 
						  DateTime createddate, 
						  boolean isselected,
						  String blockheader,
						  String communityName)
	{
		this.fname = fname;
		this.lname = lname;
		this.email = email;
		this.phone = phone;
		this.createddate = createddate;
		this.isselected = isselected;
		this.blockheader = blockheader;
		this.communityName = communityName;
		this.accountid = null;
	}

	public AccountWrapper(String accountid,
						  String fname, 
						  String lname, 
						  String email, 
						  String phone, 
						  DateTime createddate, 
						  boolean isselected,
						  String blockheader,
						  String communityName)
	{
		this.accountid = accountid;
		this.fname = fname;
		this.lname = lname;
		this.email = email;
		this.phone = phone;
		this.createddate = createddate;
		this.isselected = isselected;
		this.blockheader = blockheader;
		this.communityName = communityName;
	}

}