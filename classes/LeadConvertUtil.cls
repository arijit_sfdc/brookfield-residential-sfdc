public with sharing class LeadConvertUtil {

	public static Map<String, Schema.RecordTypeInfo> accountRecordTypeaMap = Account.SObjectType.getDescribe().getRecordTypeInfosByName();

	public LeadConvertUtil() {
		
	}

	// Convert Lead and create a new Account and Opportunity
	public static Database.LeadConvertResult convertLead(Lead lead, 
														 String convertStatus, 
														 String oppname,
														 boolean createOpportunity)
	{
		Database.LeadConvert lc = new Database.LeadConvert();
		lc.setLeadId(lead.Id);
		if(createOpportunity)
		lc.setOpportunityName(oppname);
		else
		{
			lc.setDoNotCreateOpportunity(true);
		}
		lc.setConvertedStatus(convertStatus);
		return Database.convertLead(lc);
	}

	// Convert Lead and merge to an existing account and create a new opportunity
	public static Database.LeadConvertResult convertLead(Lead lead, 
														 String convertStatus, 
														 String oppname, 
														 Account account,
														 boolean createOpportunity)
	{
		Database.LeadConvert lc = new Database.LeadConvert();
		lc.setLeadId(lead.Id);
		lc.setAccountId(account.Id);
		if(createOpportunity)
		lc.setOpportunityName(oppname);
		else
		{
			lc.setDoNotCreateOpportunity(true);
		}
		lc.setConvertedStatus(convertStatus);
		return Database.convertLead(lc);
	}

	public static Account createPersonAccount(Database.LeadConvertResult lcr, 
											  String fname, 
											  String lname, 
											  String phone, 
											  String email)
	{
		Account account = new Account();
		if(fname <> null || lname <> null)
		{
			String query;

			// Attempt to find a match if Phone or email exists
			if(phone <> null || email <> null)
			{
				SoqlBuilder soql = new SoqlBuilder()
									.selectx('Id')
									.selectx('FirstName')
									.selectx('LastName')
									.selectx('PersonContactId')
									.fromx('Account');
				AndCondition andCondition = new AndCondition();
				if(fname <> null)
					andCondition.add(new FieldCondition('FirstName', Operator.EQUALS, fname));
				if(lname <> null)
					andCondition.add(new FieldCondition('LastName', Operator.EQUALS, lname));


				OrCondition orCondition = new OrCondition();
				if(phone <> null)
					orCondition.add(new FieldCondition('Phone', Operator.EQUALS, phone));
				if(email <> null)
					orCondition.add(new FieldCondition('PersonEmail', Operator.EQUALS, email));

				soql.wherex(new AndCondition()
									.add(new SetCondition('Id', Operator.NOT_IN, new List<String>{lcr.getAccountId()})) 
									.add(andCondition)
									.add(orCondition)
									.add(new FieldCondition('isPersonAccount', Operator.EQUALS, true)));	
				soql.limitx(1);
				query = soql.toSoql();
				System.debug(LoggingLevel.INFO, '$$$$$$ Constructed query:'+query);
				for(Account accountObj : Database.query(query))
				{
					account = accountObj;
				}
				if(account.Id == null)
				{
					account = createPersonAccount(lcr, fname, lname, phone, email, true);
				}
			}
			// create a person account;
			else
			{
				account = createPersonAccount(lcr, fname, lname, phone, email, true);
			}
		}
		return account;
	}

	private static Account createPersonAccount(Database.LeadConvertResult lcr, 
											  String fname, 
											  String lname, 
											  String phone, 
											  String email,
											  boolean callagain)
	{
		Account account = new Account();
		account.RecordTypeId = accountRecordTypeaMap.get('Individual').getRecordTypeId();
		if(lname <> null)
		account.LastName = lname;
		else
		account.LastName = fname;
		account.FirstName = fname;
		account.Phone = phone;
		account.PersonEmail = email;
		insert account;
		account = [Select Id, PersonContactId from Account where Id=: account.Id];
		return account;
	}


	public static void addToOpportunityRole(String personContactId, 
											String oppId, 
											String role)
	{
		OpportunityContactRole ocr = new OpportunityContactRole();
		ocr.OpportunityId = oppId;
		ocr.ContactId = personContactId;
		ocr.Role = role;
		insert ocr;
	}  

	public static Map<String, Opportunity> getRecentOpportunities(Set<String> accountIds)
	{
		Map<String, Opportunity> accountIdOpportunityMap = new Map<String, Opportunity>();
		for(Account account : [Select Id, (Select Id, Community__r.Name from Opportunities limit 1) 
								from Account 
								where Id IN: accountIds])
		{
			if(!account.Opportunities.isEmpty())
			{
				accountIdOpportunityMap.put(account.Id, account.Opportunities[0]);
			}
		}
		return accountIdOpportunityMap;
	}
	
}