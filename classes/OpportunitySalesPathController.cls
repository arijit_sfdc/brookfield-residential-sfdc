public class OpportunitySalesPathController {
    public Opportunity opp{get; private set;}	
    public Set<String> completedStages{get; private set;}
    public Set<String> pendingStages{get; private set;}
    public OpportunitySalesPathController(ApexPages.StandardController controller)
    {
        this.opp = (Opportunity)controller.getRecord();
        this.opp = [Select Id, StageName, isWon, isClosed From Opportunity where Id=: opp.Id];
        this.completedStages = new Set<String>();
        this.pendingStages = new Set<String>();
        init();
    }
    
    private void init()
    {
        if(opp.isClosed && opp.isWon)
        {
            for(String stage : stagesIndexMap.keyset())
            {
                //String stage = stagesIndexMap.get(key);
                if(stage == 'Closed Lost')
                {
                    continue;
                }
                
                if(stage == 'Contract Signed')
                {
                    continue;
                }
                completedStages.add(stage);
            }
        }
        else if(opp.isClosed && !opp.isWon)
        {
            for(String stage : stagesIndexMap.keyset())
            {
                //String stage = stagesIndexMap.get(key);
                if(stage == 'Contract Signed')
                {
                    continue;
                }

                if(stage == 'Closed Lost')
                {
                    continue;
                }
                pendingStages.add(stage);
            }    
        }
        else
        {
            Integer completedIndex = stagesIndexMap.get(opp.StageName);
            List<String> allStages = new List<String>();
            allStages.addAll(stagesIndexMap.keyset());
            for(Integer i=0; i< completedIndex; i++)
            {
                completedStages.add(allStages[i]);
            }

            for(Integer i = completedIndex; i< allStages.size(); i++)
            {
                if(allStages[i] <> 'Closed Lost' && allStages[i] <> opp.StageName)
                    pendingStages.add(allStages[i]);
            }
            
        }
        
    }
    
    
    public Map<String, Integer> stagesIndexMap
    {
        get
        {
            Map<String, Integer> stagesIndexMap = new Map<String, Integer>();
            Schema.DescribeFieldResult fieldResult = Opportunity.StageName.getDescribe();
            List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
            Integer index = 0;
            for(Schema.PicklistEntry f : ple)
            {
                stagesIndexMap.put(f.getLabel(), index);
                index = index + 1;
            }
            return stagesIndexMap;
        }
        private set;
    }
 
}