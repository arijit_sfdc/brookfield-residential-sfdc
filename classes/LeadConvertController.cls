/**
	   @Author Bits In Glass Inc
	   @name LeadConvertController
	   @CreateDate September 13 2016
	   @Description This Class is a controller extension for VF page "ConvertLead". This class contains logic to match accounts based on Phone and Email
	   				and also handles lead conversion process.
	   				1. Converts Lead to an Individual (Person Account)
	   				2. Creates an Opportunity
	   				3. Created Person Accounts for Co-Buyers
	   				4. Relate Co Buyers to Opportunity through Contact Role.
	   @Version <1.0>
	  */

global with sharing class LeadConvertController {

	public lead leadObj{get; set;}											// Variable declaration for Lead Object Typesx
	public Map<String, String> accountMatchedMap{get; private set;}			// Variable declaration for Maop of <String, String>
	public String oppName{get; set;}										// variable declation for String
	public List<AccountWrapper> accountlist{get; set;}						// Variable declaration for List of AccountWrapper
	private Map<String, Account> accountIdMap = new Map<String, Account>();
	//private Database.LeadConvertResult lcr;
	private cobuyerAccount cobuyer;
	private Map<String, String> leadValueOrMap = new Map<String, String>();	// Variable declaration for Map of <String, String>
	private Map<String, String> leadValueAndMap = new Map<String, String>();	// Variable declaration for Map of <String, String>
	public boolean createOpportunity{get; set;}
	public List<String> errorMessages{get; private set;}

	// Constructor for the class
	public LeadConvertController(ApexPages.StandardController controller) {
		this.accountMatchedMap = new Map<String, String>();
		this.createOpportunity = true;
		this.leadObj = (Lead)controller.getRecord();
		this.leadObj = [Select Id, FirstName, LastName, Phone, Email, Community__r.Name, Region__c,
						CoBuyerFirstName__c, CoBuyerLastName__c, Co_Buyer_Phone__c, Realtor__c,
						Community__c, LeadSource, MobilePhone,
						Co_Buyer_Email__c, Name, CreatedDate from Lead where Id=: leadObj.Id];
		this.oppname = 'Opportunity for '+leadObj.Name;
		if(leadObj.Realtor__c) this.createOpportunity = false;
		if(leadObj.Phone <> null)
		{
			leadValueOrMap.put('Phone', leadObj.Phone);
		}

		if(leadObj.Email <> null)
		{
			leadValueOrMap.put('PersonEmail', leadObj.Email);
		}
		if(leadObj.FirstName <> null)
		{
			leadValueAndMap.put('FirstName', leadobj.FirstName);
		}
		leadValueAndMap.put('LastName', leadObj.LastName);

		init();
		validate();
	}

	private void validate()
	{
		this.errorMessages = new List<String>();
		if(leadObj.Region__c == null){
			errorMessages.add('Region');
		}

		if(leadObj.Community__c == null){
			errorMessages.add('Community');
		}

		if(leadObj.LeadSource == null){
			errorMessages.add('Lead Source');
		}

		if(leadObj.Email == null){
			errorMessages.add('Email');
		}

		if(leadObj.Phone == null){
			errorMessages.add('Phone');
		}

	}


	/* @Description: This method contains logic to query person accounts based on Matching Phone and Lead from a Lead Record.
	 * @arguments: None
	 * @return : none
	 */
	private void init()
	{
		Set<String> accountIds = new Set<String>();
		this.accountlist = new List<AccountWrapper>();
		System.debug(LoggingLevel.INFO, '##### content of lead value map: '+leadValueOrMap);
		System.debug(LoggingLevel.INFO, '##### content of lead value map: '+leadValueAndMap);
		String queryString = ''; 
		SoqlBuilder queryBuild = new SoqlBuilder()
								 .selectx('FirstName')
								 .selectx('LastName')
								 .selectx('Phone')
								 .selectx('PersonEmail')
								 .selectx('Name')
								 .selectx('Owner.Name')
								 .selectx('CreatedDate')
								 .fromx('Account')
								 .orderByx(new OrderBy('Phone').ascending().nullsLast())
								 .orderByx(new OrderBy('PersonEmail').ascending())
								 .orderByx(new OrderBy('LastName').ascending().nullsLast())
								 .orderByx(new OrderBy('FirstName').ascending().nullsLast());
								 /*.orderByx(new OrderBy('Email').ascending());*/
		AndCondition andCondition = new AndCondition();
		for(String fieldName : leadValueAndMap.keyset())
		{
			andCondition.add(new FieldCondition(fieldName, 
												Operator.EQUALS,
												leadValueAndMap.get(fieldName)));
		}

		OrCondition orCondition = new OrCondition();

		for(String fieldName : leadValueOrMap.keyset())
		{
			orCondition.add(new FieldCondition(fieldName, 
												Operator.EQUALS,
												leadValueOrMap.get(fieldName)));
		}						 
		queryBuild.wherex(
					new OrCondition()
					.add(andCondition)
					.add(orCondition)
		);
		
		queryString = queryBuild.toSoql() + ' limit 10';
	
		System.debug(LoggingLevel.INFO, '#### Constructed query' + queryString);
		accountlist.add(new AccountWrapper(leadobj.FirstName, 
										   leadObj.LastName, 
										   leadObj.Email, 
										   leadObj.Phone, 
										   leadObj.CreatedDate,
										   false,
										   'New Account',
										   leadobj.Community__r.Name));
		for(Account account : Database.query(queryString))
		{
			accountIdMap.put(account.Id, account);
		}
		Map<String, Opportunity> accountOppMap = LeadConvertUtil.getRecentOpportunities(accountIdMap.keyset());

		for(Account account : accountIdMap.values())
		{
			if(accountOppMap.containsKey(account.Id))
			{
				accountlist.add(new AccountWrapper(account.id,
												   account.FirstName, 
											   	   account.LastName, 
											   	   account.PersonEmail, 
											       account.Phone, 
											       account.CreatedDate,
											       false,
											       'Existing Account',
											       accountOppMap.get(account.Id).Community__r.Name));
			}
			else
			{
				accountlist.add(new AccountWrapper(account.id,
												   account.FirstName, 
											       account.LastName, 
											       account.PersonEmail, 
											       account.Phone, 
											       account.CreatedDate,
											       false,
											       'Existing Account',
											       null));
			}
			
		}
		
		this.cobuyer = new cobuyerAccount(leadObj.CoBuyerFirstName__c, 
										  leadObj.CoBuyerLastName__c, 
										  leadobj.Co_Buyer_Phone__c, 
										  leadObj.Co_Buyer_Email__c);
	}

	public List<SelectOption> getOpportunityOptions()
	{
		List<SelectOption> options = new List<SelectOption>();
		options.add(new SelectOption('Yes', 'true'));
		options.add(new SelectOption('No', 'False'));
		return options;
	}

	public PageReference convertLead()
	{
        Database.LeadConvertResult lcr;
		System.debug(LoggingLevel.INFO, 'Inside Convertlead function');
		Account selectedAccount = null;
		
		Savepoint sp = Database.setSavepoint();
		try
		{
			LeadStatus convertStatus = [SELECT Id, MasterLabel FROM LeadStatus WHERE IsConverted=true LIMIT 1];
			for(AccountWrapper account : accountlist)
			{
				if(account.isselected)
				{
					selectedAccount = accountIdMap.get(account.accountid);
				}
				
			}
			System.debug(LoggingLevel.INFO,+'#### Selected Account'+selectedAccount);
			if(selectedAccount == null){
				lcr = LeadConvertUtil.convertLead(leadObj, 
													   convertStatus.MasterLabel, 
													   oppName,
													   createOpportunity);
			}
			else
			{
				lcr = LeadConvertUtil.convertLead(leadObj, 
													   convertStatus.MasterLabel, 
													   oppName, 
													   selectedAccount,
													   createOpportunity);	
			}

			if(lcr.isSuccess()){
				Account cobuyer = LeadConvertUtil.createPersonAccount(lcr, 
																	  cobuyer.fname, 
																	  cobuyer.lname, 
																	  cobuyer.phone, 
																	  cobuyer.email);
				if(cobuyer.Id <> null && lcr.getOpportunityId() <> null)
					LeadConvertUtil.addToOpportunityRole(cobuyer.PersonContactId, 
														 lcr.getOpportunityId(), 
														 'Co-Buyer');
				return new PageReference('/'+lcr.getAccountId());
			}
			else
			{
				Database.rollback(sp);
				return null;
			}
			
			
		}
		catch(Exception ex)
		{
			return null;
		}
	}	
		
	
	private class cobuyerAccount{
		String fname;
		String lname;
		String email;
		String phone;

		cobuyerAccount(String fname, String lname, String phone, String email)
		{
			this.fname = fname;
			this.lname = lname;
			this.email = email;
			this.phone = phone;
		}
	}

}